## About
Snake Game in python3.11

### Install Python Modules
- On Debian Linux:
```bash
$ python3.11 -m pip install --break-system-packages --no-warn-script-location --user -r requirements.txt
```

- Other Linux:
```bash
$ python3.11 -m pip install --user -r requirements.txt
```

### How to use:
```bash
chmod +x snake.py
```

```bash
./snake.py
```

### Screenshots
| Game |
|------------------------------------------------------|
| ![ Photo of stream ]( game.jpg  "title" ) |
