#!/usr/bin/env python3.11

import sys
import random
try:
    import pygame
except ImportError:
    sys.exit("python3.11 -m pip install --break-system-package pygame")
try:
    import argparse
    from argparse import RawTextHelpFormatter
except:
    sys.exit("python3.11 -m pip install --break-system-package argparse")

parser = argparse.ArgumentParser(description="Snake Game",
                                 formatter_class=RawTextHelpFormatter,
                                 epilog="Examples:\n"
                                        f"{sys.argv[0]} --speed\n"
                                 )
parser.add_argument("--speed", type=int, required=False, help="Speed")
parser.add_argument("--life", type=int, required=False, help="Life")
parser.add_argument("--nos", action="store_true", required=False, help="Not increment speed")
args = parser.parse_args()

pygame.init()
pygame.display.set_caption("Snake Game")
clock = pygame.time.Clock()
width, height = 800, 600
screen = pygame.display.set_mode((width, height))

# RGB colors
black = (0, 0, 0)
red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 128, 255)
white = (255, 255, 255)
golden = (255, 223, 0)

# Snake parameters
square_size = 20
game_speed = args.speed if args.speed else 15
life = args.life if args.life else 1
advantage = 0

def generate_food():
    food_x = round(random.randrange(0, width - square_size) / float(square_size)) * float(square_size)
    food_y = round(random.randrange(0, height - square_size) / float(square_size)) * float(square_size)
    return food_x, food_y

def draw_food(size, food_x, food_y):
    pygame.draw.rect(screen, red, [food_x, food_y, size, size])

def draw_special_food(size, food_x, food_y):
    pygame.draw.rect(screen, golden, [food_x, food_y, size, size])

def draw_snake(size, pixels):
    for pixel in pixels:
        pygame.draw.rect(screen, green, [pixel[0], pixel[1], size, size])

def draw_score(score, snake_size, life):
    record = get_record()
    if score > record:
        record = score

    with open("progress.txt", "w") as file:
        file.write(f"Points: {score}\n")
        file.write(f"Lives: {life}\n")
        file.write(f"Speed: {game_speed + snake_size - 1}\n")
        file.write(f"Record: {record}\n")

    font = pygame.font.SysFont("Arial", 30)
    if args.nos:
        text = font.render(f"Points: {score} | Lives: {life} | Speed {game_speed} | Record: {record}", True, blue)
    else:
        text = font.render(f"Points: {score} | Lives: {life} | Speed {game_speed + snake_size - 1} | Record: {record}", True, blue)
    screen.blit(text, [10, 10])

def get_record():
    try:
        with open("progress.txt", "r") as file:
            lines = file.readlines()
            record = 0
            for line in lines:
                if line.startswith("Record:"):
                    record_str = line.split(":")[1].strip()
                    record = max(record, int(record_str))
            return record
    except FileNotFoundError:
        return 0
    except ValueError:
        return 0

def select_speed(key):
    if key == pygame.K_DOWN or key == pygame.K_s:
        speed_x = 0
        speed_y = square_size
    elif key == pygame.K_UP or key == pygame.K_w:
        speed_x = 0
        speed_y = -square_size
    elif key == pygame.K_RIGHT or key == pygame.K_d:
        speed_x = square_size
        speed_y = 0
    elif key == pygame.K_LEFT or key == pygame.K_a:
        speed_x = -square_size
        speed_y = 0
    elif key == pygame.K_q:
        sys.exit(0)
    try:
        return speed_x, speed_y
    except:
        speed_x = random.choice([square_size, -square_size])
        speed_y = random.choice([square_size, -square_size])
        return speed_x, speed_y

def run_game():
    global life, advantage
    game_over = False
    paused = False
    x = width / 2
    y = height / 2
    speed_x = 0
    speed_y = 0
    snake_size = 1
    pixels = []
    food_x, food_y = generate_food()

    while not game_over:
        screen.fill(black)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_over = True
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_p:
                    paused = not paused
                elif not paused:
                    speed_x, speed_y = select_speed(event.key)

        if not paused:
# Draw food
            draw_food(square_size, food_x, food_y)
            if snake_size % 100 == 0:
                draw_special_food(square_size, food_x, food_y)
                advantage += 10

# Update snake position
            if x < 0 or x >= width or y < 0 or y >= height:
                life -= 1
                if life == 0:
                    game_over = True
                else:
                    x = width / 2
                    y = height / 2

            x += speed_x
            y += speed_y

# Draw snake
            pixels.append([x, y])
            if len(pixels) > snake_size:
                del pixels[0]

# Check if the snake collided with its own body
            for pixel in pixels[:-1]:
                if pixel == [x, y]:
                    life -= 1
                    if life == 0:
                        game_over = True
                    else:
                        x = width / 2
                        y = height / 2

            draw_snake(square_size, pixels)

# Draw points
            draw_score(advantage + snake_size - 1, snake_size, life)

# Draw a blue border
            pygame.draw.rect(screen, blue, [0, 0, width, height], 1)

# Update the screen
            pygame.display.update()

# Create a new food
            if x == food_x and y == food_y:
                snake_size += 1
                food_x, food_y = generate_food()
            if args.nos:
                clock.tick(game_speed)
            else:
                clock.tick(game_speed + snake_size - 1)
        else:
            font = pygame.font.SysFont("Arial", 50)
            text = font.render("Paused", True, white)
            screen.blit(text, [width / 2 - 80, height / 2 - 25])
            pygame.display.update()

if __name__ == "__main__":
    try:
        run_game()
    except KeyboardInterrupt:
        sys.exit(0)
